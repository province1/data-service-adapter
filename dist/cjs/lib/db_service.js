"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.db_service_request = db_service_request;

var _axios = _interopRequireDefault(require("axios"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

async function db_service_request(data, service_data) {
  try {
    var result = await (0, _axios.default)({
      headers: {
        'Content-Type': 'application/json'
      },
      method: "post",
      url: service_data.url,
      auth: {
        username: service_data.username,
        password: service_data.key
      },
      data
    });
    return result.data;
  } catch (e) {
    //TODO: Unhandled Error: network error:  1645980232466 { error: true, error_code: 404, error_msg: 'Not Found' }
    if ("errno" in e) {
      // e.errno -111 network connectivity error
      if (e.errno == -111) {
        console.log("network error: ", Date.now(), {
          error: true,
          error_code: 111,
          error_msg: e.message
        });
        return {
          error: true,
          error_code: 111,
          error_msg: e.message
        };
      } else {
        // New error - not witnessed before
        console.log("network error: ", Date.now(), {
          error: true,
          error_code: e.errno,
          error_msg: e.message
        });
        return {
          error: true,
          error_code: e.errno,
          error_msg: e.message
        };
      }
    }

    if ("response" in e) {
      // Authorization failure
      if (e.response.status == 401) {
        console.log("network authorization error: ", Date.now(), {
          error: true,
          error_code: e.response.status,
          error_msg: e.response.statusText
        });
        return {
          error: true,
          error_code: e.response.status,
          error_msg: e.response.statusText
        };
      } else {
        // New error - not witnessed before
        console.log("network error: ", Date.now(), {
          error: true,
          error_code: e.response.status,
          error_msg: e.response.statusText
        });
        return {
          error: true,
          error_code: e.response.status,
          error_msg: e.response.statusText
        };
      }
    }
  }
}