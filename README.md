Middleware adapter for accessing the Database Service.


### Development
#### Install

```
npm i
```

or

```
yarn
```

#### Testing

```
npm run test
```

or

```
yarn test
```

### Use it as a package dependency
Just install it as regular package but provide a url instead of package name
```
npm install https://gitlab.com/province1/data-service-adapter
```

or 

```
yarn add https://gitlab.com/province1/data-service-adapter
```

Use normally:

```
import { database_service } from 'data-service-adapter'

or

var { database_service } = require('data-service-adapter')
```
