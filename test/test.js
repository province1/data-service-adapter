import {database_service} from '../src/index.js';

import * as chai from 'chai';
var should = chai.should();
var expect

import dotenv from "dotenv";
dotenv.config();

var db = database_service(process.env.DB_SERVICE_URL, process.env.DB_SERVICE_ID, process.env.DB_SERVICE_KEY);


var data = {
        collection: 'equipment',
        query: { serial: 0 }
}

var state_change_data = {
    
    entry_doc: {object: "bulldozer",
            mode:"g1",
            timestamp: Date.now(),
            owner: "0x54C6C1fB0f767335F6B958Df512A53bC7E3F6266".toLowerCase(),
            status: "idle",
            img_url: "https://eep.province.gg/images/bulldozer.png",
            description: "The G1 Bulldozer can clear 1 km sq of forest and gather the timber in ...",
            item: "Bulldozer (G1)",
            serial: 3,
            new_location:"78",
    },
    collection: "equipment"
}

var sum_data = {
        
        collection: "materials",
        filter: {type: "wood"}
        
        
}

describe('Test the 4 major functions: query, get current_state, insert state change, and summation aggreation (materials collection)', function() {
    
    it('Query should not return an error', async () => {
        
        var query_result = await db.data_op(data, "query")
        query_result.error.should.equal(false)

    })
    
    it('Getting the current state should not return an errors', async () => {
        
        var state_query_result = await db.data_op(data, "current_state")
        state_query_result.error.should.equal(false)

    })
    
    it('State changes should return ', async () => {
        
        var change_result = await db.data_op(state_change_data, "state_change")
        change_result.acknowledged.should.equal(true)

    })
    
    it('Aggregate sum should return a value', async () => {
        
        var agg_result = await db.data_op(sum_data, "sum_agg")
        agg_result[0].total.should.be.above(0)

    })
    
    
})



