
import { db_service_request } from "./lib/db_service.js";

const urls =  {
                current_state: "/current_state", 
                query: "/query",
                state_change: "/insert_single",
                sum_agg: "/sum_aggregate",
}

export const database_service = function (db_service_url, username, key) {
    
 return {
    data_op: async function(data, op){
        
        var service_data = {username, key, url: db_service_url + urls[op]}
        
        data.username = username
        data.key = key
        
        return db_service_request(data, service_data)
    }
}

    
}



